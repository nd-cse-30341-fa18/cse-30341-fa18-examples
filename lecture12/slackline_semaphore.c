/* slackline_semaphore.c: Slacklining example (semaphore) */

#include "thread.h"
#include <semaphore.h>

/* Constants */

const size_t CAPACITY = 3;
const size_t NTHREADS = 1<<10;

/* Global Variables */

sem_t Line;

/* Functions */

void get_on() {
    sem_wait(&Line);
}

void get_off() {
    sem_post(&Line);
}

size_t get_nslackers() {
    int nslackers;
    sem_getvalue(&Line, &nslackers);
    return CAPACITY - nslackers;
}

/* Threads */

void *	slackliner(void *arg) {
    size_t tid = (size_t)arg;
    get_on();
    printf("%lu crosses the slackline : %lu\n", tid, get_nslackers());
    get_off();
    return NULL;
}

/* Main execution */

int main(int argc, char *argv[]) {
    Thread t[NTHREADS];
    sem_init(&Line, 0, CAPACITY);

    for (size_t i = 0; i < NTHREADS; i++)
    	thread_create(&t[i], NULL, slackliner, (void *)i);
    for (size_t i = 0; i < NTHREADS; i++)
    	thread_join(t[i], NULL);
    return 0;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
