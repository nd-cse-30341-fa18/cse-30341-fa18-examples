/* salsa_night.c: Salsa night example (locks, condition variables) */

#include "thread.h"

/* Constants */

const size_t MIN_FRIENDS = 2;
const size_t NFRIENDS    = 5;

/* Global Variables */

size_t Friends = 0;
Mutex  Lock    = PTHREAD_MUTEX_INITIALIZER;
Cond   Dancing = PTHREAD_COND_INITIALIZER;

/* Threads */

void *	you_dance(void *arg) {
    size_t tid = (size_t)arg;
    mutex_lock(&Lock);

    while (Friends < MIN_FRIENDS) {
        cond_wait(&Dancing, &Lock);
    }

    printf("Thread %lu is dancing!\n", tid);

    mutex_unlock(&Lock);
    return NULL;
}

void *	friend_dance(void *arg) {
    size_t tid = (size_t)arg;
    mutex_lock(&Lock);
    Friends++;
    
    printf("Thread %lu is dancing!\n", tid);
    
    cond_signal(&Dancing);
    mutex_unlock(&Lock);
    return NULL;
}

/* Main execution */

int main(int argc, char *argv[]) {
    Thread t[NFRIENDS + 1];

    thread_create(&t[0], NULL, you_dance, (void *)0);
    for (size_t i = 1; i <= NFRIENDS; i++) {
    	thread_create(&t[i], NULL, friend_dance, (void *)i);
    }

    for (size_t i = 0; i <= NFRIENDS; i++) {
    	thread_join(t[i], NULL);
    }
    return 0;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
